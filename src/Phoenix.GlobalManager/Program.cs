﻿using Phoenix.Framework.Network;

using System;
using System.Buffers;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;

namespace Phoenix.GlobalManager
{
    class GameConnection  : NetworkConnection
    {
        private GameProtocol _protocol;

        public GameConnection(Socket socket) : base(socket)
        {
            _protocol = new GameProtocol();
            this.DataReceived += this.OnDataReceived;
            this.StartReceivingData();
            Thread.Sleep(1000);
            this.InitHandshake();
         
        }

        
        private async void InitHandshake()
        {

            var handshake = new PacketStream(0x5000);
            handshake.Write<byte>(0x01);
            _protocol.SendPacket(handshake);
            await _protocol.EncodePacketsAsync();

            var outgoing = _protocol.GetOutgoing();
            foreach (var buf in outgoing)
            {
                var mem = buf.ReadArray<byte>(buf.Length);
                await this.SendData(mem, true);
            }

            outgoing.Clear();
        }

        private async Task SendHandshakeConfirmation()
        {
            var handshake = new PacketStream(0x9000);
            handshake.Write<ushort>(0x0000);

            _protocol.SendPacket(handshake);
            await _protocol.EncodePacketsAsync();

            var outgoing = _protocol.GetOutgoing();
            foreach (var buf in outgoing)
            {
                var mem = buf.ReadArray<byte>(buf.Length);
                await this.SendData(mem, true);
            }

            outgoing.Clear();
        }

        private async Task SendServiceIdentity()
        {
            var identity = new PacketStream(0x2001);
            identity.WriteStringASCII("GatewayServer");
            identity.Write<byte>(0);

            _protocol.SendPacket(identity);
            await _protocol.EncodePacketsAsync();

            var outgoing = _protocol.GetOutgoing();
            foreach (var buf in outgoing)
            {
                var mem = buf.ReadArray<byte>(buf.Length);
                await this.SendData(mem, true);
            }

            outgoing.Clear();
        }

        private async Task SendPatchInfo()
        {
            var patchInfo = new PacketStream(0xA100);
            patchInfo.Write<byte>(1);

            _protocol.SendPacket(patchInfo);
            await _protocol.EncodePacketsAsync();

            var outgoing = _protocol.GetOutgoing();
            foreach (var buf in outgoing)
            {
                var mem = buf.ReadArray<byte>(buf.Length);
                await this.SendData(mem, true);
            }

            outgoing.Clear();
        }

        private async void OnDataReceived(INetworkConnection sender, ReadOnlyMemory<byte> data)
        {
            var seq = new ReadOnlySequence<byte>(data);
            await _protocol.DecodePacketsAsync(seq);
            var incoming = _protocol.GetIncoming();
            foreach (var packet in incoming)
            {
                Console.WriteLine("==========================");

                Console.WriteLine("Opcode: {0:X}, Length: {1}", packet.Opcode, packet.Length);
                var body = packet.ReadArray<byte>(packet.Length);
                foreach (var bt in body)
                    Console.Write("0x{0:X} ", bt);

                Console.WriteLine();

                packet.Seek(0, SeekOrigin.Begin);
                if (packet.Opcode == 0x9000)
                {
                    Console.WriteLine("Sending hs confirmation");
                    await SendHandshakeConfirmation();
                }

                if (packet.Opcode == 0x2001)
                {
                    Console.WriteLine("Name: {0}", packet.ReadStringASCII());
                    Console.WriteLine("Sending service identification");
                    await SendServiceIdentity();
                }

                if (packet.Opcode == 0x6100)
                {
                    Console.WriteLine("Sending patch info");
                    //await SendPatchInfo();
                }

                
            }

            incoming.Clear();
        }
    }

    class NetworkTest
    {
        private AsyncSocketListener _listener;


        public NetworkTest()
        {
            _listener = new AsyncSocketListener(new IPEndPoint(IPAddress.Any, 5000));
            _listener.Start();
            _listener.SocketAccepted += this.SocketListener_OnSocketAccepted;
            _listener.DispatchAcceptedSockets = true;

        }

        private async void SocketListener_OnSocketAccepted(ISocketListener sender, Socket client)
        {
            Console.WriteLine("Client accepted...");
            var conn = new GameConnection(client);
            
        }
    }
    internal class Program
    {
        private static void Main(string[] args)
        {

            //test
            System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);

            NetworkTest test = new NetworkTest();
            Console.ReadLine();
            byte[] data1 = new byte[]

            {
                0x06, 0x00, //size
                0x02, 0xA1, //opcode
                0x00, 0x00,
                0xEE, 0xFF,

                0x07, 0x00, //size
                0x01, 0x50, //opcode
                0x00, 0x00,
                0x1, 0x2, 0x3
            };

            byte[] data2 = new byte[]
            {
                0x07, 0x00, //size
                0x00, 0x90, //opcode
                0x00, 0x00,
                0x4, 0x5, 0x6
            };

            //ok that works... BUT
            GameProtocol protocol = new GameProtocol();
            ReadOnlySequence<byte> seq = new ReadOnlySequence<byte>(data1);
            protocol.DecodePacketsAsync(seq);
            seq = new ReadOnlySequence<byte>(data2);
            protocol.DecodePacketsAsync(seq);

            var incoming = protocol.GetIncoming();
            foreach (var packet in incoming)
            {
                Console.WriteLine("------------------");
                Console.WriteLine("Opcode = {0:X}", packet.Opcode);
                byte[] data = packet.ReadArray<byte>(packet.Length);
                foreach (byte bt in data)
                    Console.Write("0x{0:X} ", bt);
                Console.WriteLine();

            }

            //test
            System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);

            var testPacket1 = new PacketStream(0x9001);
            var testPacket2 = new PacketStream(0x5000);

            testPacket1.WriteStringASCII("Testing ASCII strings.");
            testPacket2.WriteArray<byte>(new byte[] { 1, 2, 3, 4, 5, 6, 7 }.AsSpan(0, 7));
            protocol.SendPacket(testPacket1);
            protocol.SendPacket(testPacket2);

            protocol.EncodePacketsAsync();

            var outgoing = protocol.GetOutgoing();
            foreach (var packet in outgoing)
            {
                Console.WriteLine("------------------");
                //Console.WriteLine("Opcode = {0:X}", packet.Opcode);
                byte[] data = packet.ReadArray<byte>(packet.Length);
                foreach (byte bt in data)
                    Console.Write("0x{0:X} ", bt);
                Console.WriteLine();

            }

            Console.ReadLine();
            return;
        }
    }
}