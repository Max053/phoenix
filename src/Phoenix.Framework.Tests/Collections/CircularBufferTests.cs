using Microsoft.VisualStudio.TestTools.UnitTesting;

using Phoenix.Framework.Collections;

namespace Phoenix.Framework.Tests
{
    /// <summary>
    /// Tests for <see cref="Phoenix.Framework.Collections.CircularBuffer{T}"/>.
    /// </summary>
    [TestClass]
    public class CircularBufferTests
    {
        /// <summary>
        /// Checks if the circular buffer capacity does not go over the max capacity and
        /// the oldest element is propertly removed.
        /// </summary>
        [TestMethod]
        public void PushingOverCapacityShouldCutTail()
        {
            const int itemCount = 5;
            var buffer = new CircularBuffer<int>(itemCount);
            for (int i = 0; i < (itemCount + 1); i++)
                buffer.Push(i);

            Assert.AreEqual(buffer.Count, itemCount);
        }

        /// <summary>
        /// Checks if the snapshot operation does not bug the circular buffer.
        /// </summary>
        [TestMethod]
        public void SnapshottingShouldNotAffectBuffer()
        {
            const int itemCount = 5;
            var buffer = new CircularBuffer<int>(itemCount);
            for (int i = 0; i < itemCount; i++)
                buffer.Push(i);

            int[] snapshot = buffer.GetSnapshot();

            Assert.AreEqual(buffer.Count, snapshot.Length);

            buffer.Push(1337);

            Assert.AreEqual(snapshot[itemCount - 1], 4);

            //Now after we added 1337 to the end of buffer the tail should be 1 and front 1337
            snapshot = buffer.GetSnapshot();

            Assert.AreEqual(snapshot[0], 1);
            Assert.AreEqual(snapshot[itemCount - 1], 1337);
        }
    }
}