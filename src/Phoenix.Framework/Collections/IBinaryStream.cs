﻿using System;
using System.IO;

namespace Phoenix.Framework.Collections
{
    /// <summary>
    /// Binary stream mode.
    /// </summary>
    public enum BinaryStreamMode
    {
        /// <summary>
        /// The stream is in read-only mode.
        /// </summary>
        Read,

        /// <summary>
        /// The stream is in write-only mode.
        /// </summary>
        Write
    }

    /// <summary>
    /// Interface for a binary stream.
    /// </summary>
    public interface IBinaryStream : IDisposable
    {
        /// <summary>
        /// Gets the binary stream mode.
        /// </summary>
        BinaryStreamMode Mode { get; }

        /// <summary>
        /// Gets the stream length.
        /// </summary>
        int Length { get; }

        /// <summary>
        /// Gets the stream offset.
        /// </summary>
        int Offset { get; }

        /// <summary>
        /// Switches the stream mode.
        /// </summary>
        /// <param name="mode">The new mode.</param>
        void SwitchMode(BinaryStreamMode mode);

        /// <summary>
        /// Seeks the stream to given offset.
        /// </summary>
        /// <param name="offset">The offset.</param>
        /// <param name="seekOrigin">The seek origin.</param>
        void Seek(int offset, SeekOrigin seekOrigin = SeekOrigin.Begin);

        /// <summary>
        /// Reads an value from the stream.
        /// </summary>
        /// <typeparam name="T">The type of value.</typeparam>
        /// <returns>The value.</returns>
        T Read<T>()
            where T : unmanaged;

        /// <summary>
        /// Reads an <see cref="Span{T}"/> from the stream.
        /// </summary>
        /// <typeparam name="T">The type of array.</typeparam>
        /// <param name="destination">The destination span.</param>
        void ReadArray<T>(in Span<T> destination)
            where T : unmanaged;

        /// <summary>
        /// Reads a <see cref="Memory{T}"/> from the stream.
        /// </summary>
        /// <typeparam name="T">The type of array.</typeparam>
        /// <param name="destination">The array.</param>
        void ReadArray<T>(in Memory<T> destination)
            where T : unmanaged;

        /// <summary>
        /// Reads an array from the stream.
        /// </summary>
        /// <typeparam name="T">The type of array.</typeparam>
        /// <param name="count">The count.</param>
        /// <returns>The array.</returns>
        T[] ReadArray<T>(int count)
            where T : unmanaged;

        /// <summary>
        /// Writes a value to the stream.
        /// </summary>
        /// <typeparam name="T">The type of value.</typeparam>
        /// <param name="value">The value.</param>
        void Write<T>(ref T value)
            where T : unmanaged;

        /// <summary>
        /// Writes a value to the stream.
        /// </summary>
        /// <typeparam name="T">The type of value.</typeparam>
        /// <param name="value">The value.</param>
        void Write<T>(T value)
            where T : unmanaged;

        /// <summary>
        /// Writes an array to the stream.
        /// </summary>
        /// <typeparam name="T">The type of array.</typeparam>
        /// <param name="source">The source.</param>
        void WriteArray<T>(in ReadOnlySpan<T> source)
            where T : unmanaged;

        /// <summary>
        /// Writes an array to the stream.
        /// </summary>
        /// <typeparam name="T">The type of array.</typeparam>
        /// <param name="source">The source.</param>
        void WriteArray<T>(in ReadOnlyMemory<T> source)
            where T : unmanaged;

        /// <summary>
        /// Writes an array to the stream.
        /// </summary>
        /// <typeparam name="T">The type of array.</typeparam>
        /// <param name="source">The source array.</param>
        /// <param name="offset">The offset.</param>
        /// <param name="count">The count.</param>
        void WriteArray<T>(in T[] source, int offset, int count)
            where T : unmanaged;
    }
}
