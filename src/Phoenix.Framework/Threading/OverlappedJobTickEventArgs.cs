﻿using System;

namespace Phoenix.Framework.Threading
{
    /// <summary>
    /// The overlapped job tick event args.
    /// </summary>
    public sealed class OverlappedJobTickEventArgs : EventArgs
    {
        /// <summary>
        /// The time elapsed since last execution.
        /// </summary>
        public TimeSpan DeltaTime { get; private set; }

        /// <summary>
        /// Initializes a new instance of <see cref="OverlappedJobTickEventArgs"/>.
        /// </summary>
        /// <param name="deltaTime">The delta time.</param>
        public OverlappedJobTickEventArgs(TimeSpan deltaTime)
        {
            this.DeltaTime = deltaTime;
        }

        /// <summary>
        /// Prevents initialization of new instance of <see cref="OverlappedJobTickEventArgs"/>.
        /// </summary>
        private OverlappedJobTickEventArgs()
        {
        }
    }
}