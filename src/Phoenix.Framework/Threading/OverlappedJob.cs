﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace Phoenix.Framework.Threading
{
    /// <summary>
    /// The overlapped job implementation.
    /// </summary>
    public class OverlappedJob : IManageableTask
    {
        #region Private & static

        private TimeSpan _interval;
        private Task _workerTask;
        private CancellationTokenSource _workerCancellationTokenSource;
        private Stopwatch _tickDeltaTimeWatch;
        private ManualResetEventSlim _workerTaskStartedMRE;
        private bool _terminateOnTickEventException;
        private bool _isPaused;
        private bool _isDelayedStart;
        private bool _isDisposed;

        #endregion Private & static

        #region Constructors

        /// <summary>
        /// Initialies a new instance of <see cref="OverlappedJob"/>.
        /// </summary>
        public OverlappedJob()
        {
        }

        #endregion Constructors

        #region OverlappedJob

        /// <summary>
        /// The delegate for the worker tick event.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The event args. See <see cref="OverlappedJobTickEventArgs"/>.</param>
        public delegate void OverlappedJobTickEventHandler(OverlappedJob sender, OverlappedJobTickEventArgs e);

        /// <summary>
        /// Fired once the worker task context interval elapses.
        /// </summary>
        public event OverlappedJobTickEventHandler OnTick = (s, e) => { };

        /// <summary>
        /// The event that gets fired when the task is terminated.
        /// </summary>
        public event TaskTerminatedEventHandler OnTerminated = (s, e) => { };

        /// <summary>
        /// Configures the overlapped job instance.
        /// </summary>
        /// <param name="isDelayedStart">The flag indating whether the first execution should be delayed by the interval.</param>
        /// <param name="interval">The interval between firing <see cref="OverlappedJob.OnTick"/> event handlers.</param>
        /// <param name="terminateOnTickEventException">Determines if the job should be terminated
        /// if tick event handler fired an exception.</param>
        public void Configure(bool isDelayedStart, TimeSpan interval, bool terminateOnTickEventException)
        {
            _isDelayedStart = isDelayedStart;
            _interval = interval;
            _workerCancellationTokenSource = new CancellationTokenSource();
            _tickDeltaTimeWatch = new Stopwatch();
            _workerTaskStartedMRE = new ManualResetEventSlim(false);
            _terminateOnTickEventException = terminateOnTickEventException;
            _isPaused = false;
            _isDisposed = false;

            this.IsRunning = false;
        }

        /// <summary>
        /// Pauses firing of events.
        /// </summary>
        public void Pause() => _isPaused = true;

        /// <summary>
        /// Resumes firing of events.
        /// </summary>
        public void Resume() => _isPaused = false;

        /// <summary>
        /// Gets the tick delta time.
        /// </summary>
        /// <returns>The delta time.</returns>
        public TimeSpan GetTickDelta()
        {
            //NOTE: Negatoation here @ delta time watch elapsed milliseconds.
            var previousExecution = DateTime.Now.AddMilliseconds(-_tickDeltaTimeWatch.ElapsedMilliseconds);
            return (DateTime.Now - previousExecution);
        }

        /// <summary>
        /// The worker task context.
        /// </summary>
        private async void WorkerTaskContext()
        {
            _workerTaskStartedMRE.Set();

            while (this.IsRunning && !_workerCancellationTokenSource.IsCancellationRequested)
            {
                //Delayed start means that we have to wait BEFORE executing the handlers.
                if (_isDelayedStart)
                    await Task.Delay((int)_interval.TotalMilliseconds);

                if (!_isPaused)
                {
                    try
                    {
                        this.OnTick?.Invoke(
                            this, new OverlappedJobTickEventArgs(this.GetTickDelta()));
                    }
                    catch (Exception ex)
                    {
                        if (_terminateOnTickEventException)
                        {
                            this.Terminate(TaskTerminationReason.HandlerThrewException, ex);
                            break;
                        }
                    }

                    _tickDeltaTimeWatch.Restart();
                }

                if (!_isDelayedStart)
                    await Task.Delay((int)_interval.TotalMilliseconds);
            }
        }

        #endregion OverlappedJob

        #region IManageableTask

        /// <summary>
        /// Gets the flag indicating whether the task is currently running.
        /// </summary>
        public bool IsRunning { get; private set; }

        /// <summary>
        /// Starts the task execution.
        /// </summary>
        /// <param name="waitForStart">Indicates if the method will block until task is actually started.</param>
        public void Run(bool waitForStart)
        {
            if (this.IsRunning)
                throw new InvalidOperationException("The underlying task is already running.");

            //The flag must be set before task was started.
            this.IsRunning = true;

            //A new cancellation task token must be created.
            _workerCancellationTokenSource = new CancellationTokenSource();

            _workerTask = Task.Factory.StartNew(
                this.WorkerTaskContext, _workerCancellationTokenSource.Token,
                TaskCreationOptions.LongRunning, TaskScheduler.Default);

            if (waitForStart)
                _workerTaskStartedMRE.Wait();
        }

        /// <summary>
        /// Terminates the task execution.
        /// </summary>
        /// <param name="reason">The termination reason.</param>
        /// <param name="ex">Exception (if any).</param>
        public void Terminate(TaskTerminationReason reason, Exception ex = null)
        {
            _workerCancellationTokenSource.Cancel();
            _workerTaskStartedMRE.Reset();

            this.IsRunning = false;

            switch (reason)
            {
                case TaskTerminationReason.ByUser:
                    this.OnTerminated?.Invoke(this, new TaskTerminatedEventArgs(reason));
                    break;

                case TaskTerminationReason.HandlerThrewException:
                    {
                        if (ex == default(Exception))
                        {
                            throw new ArgumentNullException(
                                $"This termination reason ({reason.ToString()} requires exception as argument."
                                );
                        }

                        this.OnTerminated?.Invoke(
                            this, new TaskTerminatedEventArgs(ex)
                            );
                    }
                    break;

                default:
                    throw new NotImplementedException($"Unknown manageable task termination reason: {reason.ToString()}.");
            }
        }

        #endregion IManageableTask

        #region IDisposable

        /// <summary>
        /// The <see cref="IDisposable.Dispose"/> implementation.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// The <see cref="IDisposable.Dispose"/> implementation.
        /// </summary>
        /// <param name="disposing"></param>
        public void Dispose(bool disposing)
        {
            if (_isDisposed)
                return;

            if (disposing)
            {
                _workerCancellationTokenSource?.Dispose();
                _workerTask?.Dispose();
                _workerTaskStartedMRE?.Dispose();

                _workerCancellationTokenSource = null;
                _workerTask = null;
                _workerTaskStartedMRE = null;
            }

            _isDisposed = true;
        }

        #endregion IDisposable
    }
}