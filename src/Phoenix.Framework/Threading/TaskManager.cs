﻿using System;
using System.Collections.Generic;

namespace Phoenix.Framework.Threading
{
    /// <summary>
    /// The task manager base.
    /// </summary>
    /// <typeparam name="TManageableTask">The type of manageable task.</typeparam>
    public abstract class TaskManager<TManageableTask> : ITaskManager<TManageableTask>
        where TManageableTask : IManageableTask
    {
        #region Private & static

        private List<TManageableTask> _tasks;

        #endregion Private & static

        #region Constructors

        /// <summary>
        /// Initializes a new instance of <see cref="TaskManager{TManageableTask}"/>.
        /// </summary>
        public TaskManager()
        {
            _tasks = new List<TManageableTask>();
        }

        #endregion Constructors

        #region TaskManager

        protected ICollection<TManageableTask> GetTasks() => _tasks;

        /// <summary>
        /// Registers a task.
        /// </summary>
        /// <param name="task">The task.</param>
        protected void Register(TManageableTask task) => _tasks.Add(task);

        /// <summary>
        /// Unregisters a task.
        /// </summary>
        /// <param name="task">The task.</param>
        protected void Unregister(TManageableTask task) => _tasks.Remove(task);

        #endregion TaskManager

        #region ITaskManager

        /// <summary>
        /// Starts execution of given task.
        /// </summary>
        /// <param name="task">The task.</param>
        /// <param name="waitForStart">Determines whether we block until the underlying task is actally started.</param>
        public void Run(TManageableTask task, bool waitForStart)
        {
            if (!_tasks.Contains(task))
                throw new ArgumentException("Trying to run unregistered task.");

            task.Run(waitForStart);
        }

        /// <summary>
        /// Terminates the given task.
        /// </summary>
        /// <param name="task">The task.</param>
        /// <param name="reason">The task termination reason.</param>
        /// <param name="ex">The exception (if any).</param>
        public void Terminate(TManageableTask task, TaskTerminationReason reason, Exception ex = null)
        {
            if (!_tasks.Contains(task))
                throw new ArgumentException("Trying to terminate unregistered task.");

            task.Terminate(reason, ex);
            this.Unregister(task);
        }

        /// <summary>
        /// Terminates all tasks.
        /// </summary>
        public void TerminateAll()
        {
            foreach (var task in _tasks)
                task.Terminate(TaskTerminationReason.ByUser);

            for (int i = 0; i < _tasks.Count; i++)
                this.Unregister(_tasks[i]);
        }

        #endregion ITaskManager
    }
}