﻿using System;

namespace Phoenix.Framework.Threading
{
    /// <summary>
    /// The kind of scheduled job event.
    /// </summary>
    public enum ScheduledJobEventKind
    {
        /// <summary>
        /// The job was started.
        /// </summary>
        Started,

        /// <summary>
        /// The job execution is over.
        /// </summary>
        Ended,
    }

    /// <summary>
    /// The event args for <see cref="ScheduledJob"/>.
    /// </summary>
    public class ScheduledJobEventArgs : EventArgs
    {
        /// <summary>
        /// The kind of event.
        /// </summary>
        public ScheduledJobEventKind Kind { get; private set; }

        /// <summary>
        /// Initializes a new instance of <see cref="ScheduledJobEventArgs"/>.
        /// </summary>
        /// <param name="kind">The event kind.</param>
        public ScheduledJobEventArgs(ScheduledJobEventKind kind)
        {
            this.Kind = kind;
        }

        /// <summary>
        /// Prevents initialization of <see cref="ScheduledJobEventArgs"/> instance.
        /// </summary>
        private ScheduledJobEventArgs()
        {
        }
    }
}