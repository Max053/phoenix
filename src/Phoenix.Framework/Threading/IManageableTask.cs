﻿using System;

namespace Phoenix.Framework.Threading
{
    /// <summary>
    /// The interface for manageable task.
    /// </summary>
    public interface IManageableTask : IDisposable
    {
        /// <summary>
        /// Gets the flag indicating whether the task is currently running.
        /// </summary>
        bool IsRunning { get; }

        /// <summary>
        /// Starts the task execution.
        /// </summary>
        /// <param name="waitForStart">Indicates if the method will block until task is actually started.</param>
        void Run(bool waitForStart);

        /// <summary>
        /// Terminates the task execution.
        /// </summary>
        /// <param name="reason">The termination reason.</param>
        /// <param name="ex">Exception (if any).</param>
        void Terminate(TaskTerminationReason reason, Exception ex = null);
    }
}