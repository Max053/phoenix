﻿using System;

namespace Phoenix.Framework.Threading
{
    /// <summary>
    /// The task manager interface.
    /// </summary>
    public interface ITaskManager<TManageableTask> where
        TManageableTask : IManageableTask
    {
        /// <summary>
        /// Starts execution of given task.
        /// </summary>
        /// <param name="task">The task.</param>
        /// <param name="waitForStart">Determines whether we block until the underlying task is actally started.</param>
        void Run(TManageableTask task, bool waitForStart);

        /// <summary>
        /// Terminates the given task.
        /// </summary>
        /// <param name="task">The task.</param>
        /// <param name="reason">The task termination reason.</param>
        /// <param name="ex">The exception if any.</param>
        void Terminate(TManageableTask task, TaskTerminationReason reason, Exception ex = null);

        /// <summary>
        /// Terminates all tasks.
        /// </summary>
        void TerminateAll();
    }
}