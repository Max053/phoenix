﻿using System;

namespace Phoenix.Framework.Threading
{
    /// <summary>
    /// The overlapped job manager.
    /// </summary>
    public class OverlappedJobManager : TaskManager<OverlappedJob>
    {
        /// <summary>
        /// Creats an an <see cref="OverlappedJob"/> & registers it to the task manager.
        /// </summary>
        /// <param name="isDelayedStart">Indicates whether the first start has to be delayed (by interval).</param>
        /// <param name="interval">The interval.</param>
        /// <returns></returns>
        public OverlappedJob CreateJob(bool isDelayedStart, TimeSpan interval)
        {
            var job = new OverlappedJob();
            job.Configure(isDelayedStart, interval, false);
            base.Register(job);
            return job;
        }
    }
}