﻿using System.Net.Sockets;

namespace Phoenix.Framework.Network
{
    /// <summary>
    /// Socket extensions.
    /// </summary>
    public static class SocketExtensions
    {
        /// <summary>
        /// Silently closes the socket without throwing any exceptions.
        /// </summary>
        /// <param name="socket">The socket.</param>
        public static void CloseNoThrow(this Socket socket)
        {
            try
            {
                socket.Close();
            }
            catch
            {
                //Ignored
            }
        }
    }
}
