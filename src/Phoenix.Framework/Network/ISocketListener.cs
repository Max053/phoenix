﻿using System;
using System.Net.Sockets;

namespace Phoenix.Framework.Network
{
    /// <summary>
    /// Socket listener startup result.
    /// </summary>
    public enum SocketListenerStartupResult
    {
        /// <summary>
        /// Binding socket failed.
        /// </summary>
        BindGeneralFailure,

        /// <summary>
        /// Failed to start listening.
        /// </summary>
        ListenGeneralFailure,

        /// <summary>
        /// Access denied (running as administrator?).
        /// </summary>
        InsufficientPrivileges,

        /// <summary>
        /// Successfully started.
        /// </summary>
        Success
    }

    /// <summary>
    /// The socket accepted event handler delegate.
    /// </summary>
    /// <param name="sender">The sender.</param>
    /// <param name="client">The client socket.</param>
    public delegate void SocketAcceptedEventHandler(ISocketListener sender, Socket client);

    /// <summary>
    /// Interface for socket listener.
    /// </summary>
    public interface ISocketListener : IDisposable
    {
        /// <summary>
        /// Gets the flag indicating if the socket listener is running.
        /// </summary>
        bool Running { get; }

        /// <summary>
        /// Gets the flag indicating if <see cref="SocketAccepted"/> event will
        /// be fired once client socket is accepted.
        /// </summary>
        bool DispatchAcceptedSockets { get; }

        /// <summary>
        /// Fired once a client socket is accepted.
        /// </summary>
        event SocketAcceptedEventHandler SocketAccepted;

        /// <summary>
        /// Binds the server socket and starts listening for incoming client connections.
        /// </summary>
        /// <returns>The startup result. See <see cref="SocketListenerStartupResult"/>.</returns>
        SocketListenerStartupResult Start();

        /// <summary>
        /// Shuts the listener down.
        /// </summary>
        void Terminate();
    }
}
