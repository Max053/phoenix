﻿using Phoenix.Framework.Collections;

namespace Phoenix.Framework.Network
{
    /// <summary>
    /// Interface for a packet stream.
    /// </summary>
    public interface IPacketStream : IBinaryStream
    {
        /// <summary>
        /// Gets the opcode.
        /// </summary>
        ushort Opcode { get; }

        /// <summary>
        /// Gets the flag indicating whether the packet is massive.
        /// </summary>
        bool Massive { get; }

        /// <summary>
        /// Gets the flag indicating whether the packet is encrypted.
        /// </summary>
        bool Encrypted { get; }

        /// <summary>
        /// Reads an ASCII string from the stream.
        /// </summary>
        /// <param name="codepage">The codepage.</param>
        /// <returns>The string.</returns>
        string ReadStringASCII(int codepage = 1251);

        /// <summary>
        /// Writes an ASCII string to the stream.
        /// </summary>
        /// <param name="str">The string.</param>
        /// <param name="codepage">The codepage.</param>
        void WriteStringASCII(string str, int codepage = 1251);

        /// <summary>
        /// Reads an unicode string from the stream.
        /// </summary>
        /// <returns>The string.</returns>
        string ReadStringUnicode();

        /// <summary>
        /// Writes an unicode string to the stream.
        /// </summary>
        /// <param name="str"></param>
        void WriteStringUnicode(string str);

        /// <summary>
        /// Gets all the bytes inside the stream.
        /// </summary>
        /// <returns></returns>
        byte[] GetBytes();

        /// <summary>
        /// Convert the packet into binary stream packet.
        /// </summary>
        /// <returns></returns>
        BinaryStream ToBinaryStream();

        /// <summary>
        /// Create the packet from the binary stream.
        /// </summary>
        void ParseBinaryStream(byte[] stream);
    }
}
