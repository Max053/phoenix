﻿using System;

namespace Phoenix.Framework.Network
{
    /// <summary>
    /// Reason for network connection termination.
    /// </summary>
    public enum ConnectionTerminationReason
    {
        /// <summary>
        /// Terminated due to error in recv logic.
        /// </summary>
        ReceiveDataFailed,

        /// <summary>
        /// Terminated due to error while filling send pipe.
        /// </summary>
        SendDataFailed,

        /// <summary>
        /// Terminated due to error flushing pending outgoing data.
        /// </summary>
        FlushOutgoingFailed,

        /// <summary>
        /// Normal termination by user request.
        /// </summary>
        TerminatedByUser,
    }

    /// <summary>
    /// Connection termination event args for INetworkConnection.
    /// </summary>
    public class ConnectionTerminatedEventArgs : EventArgs
    {
        /// <summary>
        /// Gets the connection termination reason.
        /// </summary>
        public ConnectionTerminationReason Reason { get; private set; }

        /// <summary>
        /// Gets the exception (if any).
        /// </summary>
        public Exception Exception { get; private set; }

        /// <summary>
        /// Initializes a new instance of <see cref="ConnectionTerminatedEventArgs"/>.
        /// </summary>
        /// <param name="reason">The connection termination reason.</param>
        /// <param name="exception">The exception (if any).</param>
        public ConnectionTerminatedEventArgs(ConnectionTerminationReason reason, Exception exception = null)
        {
            this.Reason = reason;
            this.Exception = exception;
        }

        /// <summary>
        /// Prevents initialization of <see cref="ConnectionTerminatedEventArgs"/> without valid
        /// constructor arguments supplied.
        /// </summary>
        private ConnectionTerminatedEventArgs()
        {

        }
    }
}