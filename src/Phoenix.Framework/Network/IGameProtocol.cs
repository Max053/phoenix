﻿using Phoenix.Framework.Collections;
using System.Buffers;
using System.Collections.Concurrent;
using System.Threading.Tasks;

namespace Phoenix.Framework.Network
{
    /// <summary>
    /// Interface for game protocol.
    /// </summary>
    public interface IGameProtocol
    {
        /// <summary>
        /// Decodes raw data.
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        Task DecodePacketsAsync(ReadOnlySequence<byte> data);

        /// <summary>
        /// Encodes packets queued by <see cref="SendPacket"/> method for outgoing.
        /// </summary>
        /// <returns></returns>
        Task EncodePacketsAsync();

        /// <summary>
        /// Enqueues packet for encoding & sending.
        /// </summary>
        /// <param name="packet">The packet.</param>
        void SendPacket(PacketStream packet);

        /// <summary>
        /// Gets the packets that were decoded and need to be processed.
        /// </summary>
        /// <returns>Queue of packet streams for processing.</returns>
        ConcurrentQueue<PacketStream> GetIncoming();

        /// <summary>
        /// Gets the buffers ready to be sent to the network. 
        /// </summary>
        /// <returns>Queue of binary streams for sending.</returns>
        ConcurrentQueue<BinaryStream> GetOutgoing();
    }
}
