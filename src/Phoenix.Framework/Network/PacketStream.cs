﻿using Phoenix.Framework.Collections;
using System;
using System.IO;
using System.Text;

namespace Phoenix.Framework.Network
{
    /// <summary>
    /// Packet stream implementation.
    /// </summary>
    public class PacketStream : BinaryStream, IPacketStream
    {
        #region SSA Legacy constructors

#if SSA_LEGACY

        /// <summary>
        /// Initializes a new instance of <see cref="PacketStream"/>.
        /// </summary>
        /// <param name="opcode">The opcode.</param>
        /// <param name="encrypted">The encryption flag.</param>
        /// <param name="massive">The massive flag.</param>
        /// <param name="source">The source array.</param>
        public PacketStream(ushort opcode, bool encrypted, bool massive, byte[] source) :
            this(opcode, encrypted, massive)
        {
            this.SwitchMode(BinaryStreamMode.Write);
            this.WriteArray<byte>(source.AsSpan());
        }

        /// <summary>
        /// Initializes a new instance of <see cref="PacketStream"/>.
        /// </summary>
        /// <param name="opcode">The opcode.</param>
        /// <param name="encrypted">The encryption flag.</param>
        /// <param name="massive">The massive flag.</param>
        /// <param name="source">The source array.</param>
        /// <param name="offset">The offset.</param>
        /// <param name="count">The count of bytes.</param>
        public PacketStream(ushort opcode, bool encrypted, bool massive, byte[] source, int offset, int count) :
            this(opcode, encrypted, massive)
        {
            if ((offset + count) > source.Length)
                throw new IndexOutOfRangeException("Source byte array offset/length out of bounds.");

            this.SwitchMode(BinaryStreamMode.Write);
            this.WriteArray<byte>(source.AsSpan(offset, count));
        }

#endif

        #endregion

        #region SSA Legacy methods

#if SSA_LEGACY

        /// <summary>
        /// Switches the packet stream to read mode and seeks to its beginning.
        /// </summary>
        public void Lock()
        {
            this.SwitchMode(BinaryStreamMode.Read);
            this.Seek(0, SeekOrigin.Begin);
        }

#endif

        #endregion

        #region Constructors & destructor

        /// <summary>
        /// Initializes a new instance of <see cref="PacketStream"/>.
        /// </summary>
        /// <param name="opcode">The opcode.</param>
        public PacketStream(ushort opcode) :
            base()
        {
            this.Opcode = opcode;
            this.Encrypted = false;
            this.Massive = false;
        }

        /// <summary>
        /// Initializes a new instance of <see cref="PacketStream"/>.
        /// </summary>
        /// <param name="opcode">The opcode.</param>
        /// <param name="encrypted">The encryption flag.</param>
        public PacketStream(ushort opcode, bool encrypted) :
            base()
        {
            this.Opcode = opcode;
            this.Encrypted = encrypted;
            this.Massive = false;
        }

        /// <summary>
        /// Initializes a new instance of <see cref="PacketStream"/>.
        /// </summary>
        /// <param name="opcode">The opcode.</param>
        /// <param name="encrypted">The encryption flag.</param>
        /// <param name="massive">The massive flag.</param>
        public PacketStream(ushort opcode, bool encrypted, bool massive) :
            base()
        {
            if (encrypted && massive)
            {
                throw new InvaildPacketStreamException("The packet cannot be both encrypted and massive at the same time!");
            }

            this.Opcode = opcode;
            this.Encrypted = encrypted;
            this.Massive = massive;
        }

        #endregion

        #region IPacketStream

        /// <inheritdoc />
        public ushort Opcode { get; private set; }

        /// <inheritdoc />
        public bool Encrypted { get; private set; }

        /// <inheritdoc />
        public bool Massive { get; private set; }

        /// <inheritdoc />
        public string ReadStringASCII(int codepage = 1251)
        {
            if (this.Mode != BinaryStreamMode.Read)
                throw new InvalidOperationException("The stream is not in read mode.");

            var length = this.Read<ushort>();
            if (length > short.MaxValue)
                throw new IndexOutOfRangeException("Strings longer than short.MaxValue are not supported.");

            if ((this.Length - this.Offset) < length)
                throw new IndexOutOfRangeException("Cannot read beyond the stream bounds.");

            var encoding = Encoding.GetEncoding(codepage);
            var buffer = this.ReadArray<byte>(length);

            return encoding.GetString(buffer);
        }

        /// <inheritdoc />
        public void WriteStringASCII(string str, int codepage = 1251)
        {
            if (this.Mode != BinaryStreamMode.Write)
                throw new InvalidOperationException("The stream is not in write mode.");

            if (str.Length > short.MaxValue)
                throw new IndexOutOfRangeException("Strings longer than short.MaxValue are not supported.");

            var encoding = Encoding.GetEncoding(codepage);

            this.Write<ushort>((ushort)str.Length);
            this.WriteArray<byte>(encoding.GetBytes(str).AsSpan());
        }

        /// <inheritdoc />
        public string ReadStringUnicode()
        {
            if (this.Mode != BinaryStreamMode.Read)
                throw new InvalidOperationException("The stream is not in read mode.");

            var length = this.Read<ushort>();
            if (length > short.MaxValue)
                throw new IndexOutOfRangeException("Strings longer than short.MaxValue are not supported.");


            //NOTE: Real string length (bytes) is actually two times bigger because unicode uses 2 bytes per
            //symbol, but SRO uses the symbol count, not the byte count.
            if ((this.Length - this.Offset) < (length * 2))
                throw new IndexOutOfRangeException("Cannot read beyond the stream bounds.");

            return Encoding.Unicode.GetString(this.ReadArray<byte>(length * 2));
        }

        /// <inheritdoc />
        public void WriteStringUnicode(string str)
        {
            if (this.Mode != BinaryStreamMode.Write)
                throw new InvalidOperationException("The stream is not in write mode.");

            if (str.Length > short.MaxValue)
                throw new IndexOutOfRangeException("Strings longer than short.MaxValue are not supported.");

            //NOTE: Real string length (bytes) is actually two times bigger because unicode uses 2 bytes per
            //symbol, but SRO uses the symbol count, not the byte count.
            this.Write<ushort>((ushort)str.Length);
            this.WriteArray<byte>(Encoding.Unicode.GetBytes(str).AsSpan());
        }

        /// <inheritdoc />
        public byte[] GetBytes()
        {
            // Save current mode and offset.
            var oldMode = this.Mode;
            var oldOffset = this.Offset;

            // Make sure that the stream is readable and reset the offset to zero.
            this.SwitchMode(BinaryStreamMode.Read);
            this.Seek(0, SeekOrigin.Begin);

            // Read the body that we want!
            var body = this.ReadArray<byte>(this.Length);

            // Restore the old mode and offset.
            this.SwitchMode(oldMode);
            this.Seek(oldOffset, SeekOrigin.Begin);

            return body;
        }

        /// <inheritdoc />
        public BinaryStream ToBinaryStream()
        {
            // Save current mode and offset.
            var oldMode = this.Mode;
            var oldOffset = this.Offset;

            // Make sure that the stream is readable and reset the offset to zero.
            this.SwitchMode(BinaryStreamMode.Read);
            this.Seek(0, SeekOrigin.Begin);

            // Create the stream and fill it.
            var rawStream = new BinaryStream();
            rawStream.Write<ushort>((ushort)(this.Length));
            rawStream.Write<ushort>(this.Opcode);
            rawStream.Write<ushort>(0x0000);
            byte[] body = this.ReadArray<byte>(this.Length);

            rawStream.WriteArray<byte>(body.AsSpan(0, body.Length));

            rawStream.SwitchMode(BinaryStreamMode.Read);
            rawStream.Seek(0, SeekOrigin.Begin);

            // Restore the old mode and offset.
            this.SwitchMode(oldMode);
            this.Seek(oldOffset, SeekOrigin.Begin);

            return rawStream;
        }

        /// <inheritdoc />
        public void ParseBinaryStream(byte[] stream)
        {
            // TODO
        }

        #endregion
    }

    public class InvaildPacketStreamException : Exception
    {
        public InvaildPacketStreamException(string message) : base(message)
        {

        }
        public InvaildPacketStreamException(string message, Exception innerException) : base(message, innerException)
        {

        }
    }
}
