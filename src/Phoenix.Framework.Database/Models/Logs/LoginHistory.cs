﻿using Phoenix.Framework.Database.Models.Account;
using System;
using System.Collections.Generic;
using System.Text;

namespace Phoenix.Framework.Database.Models.Logs
{
    public class LoginHistory
    {
        public int ID { get; set; }
        public User User { get; set; }
        public DateTime DateTime { get; set; }
        public string IP { get; set; }
        public string HWID { get; set; }
    }
}
