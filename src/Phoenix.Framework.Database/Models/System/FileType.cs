﻿namespace Phoenix.Framework.Database.Models.System
{
    public enum FileType : byte
    {
        Unknown = 0,
        Texture = 8
    }
}