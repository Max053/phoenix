﻿using System.Collections.Generic;

namespace Phoenix.Framework.Database.Models.Account
{
    public class SecurityGroup : Security
    {
        public IEnumerable<SecurityGroupPolicies> SecurityGroupPolicies { get; set; }
        public IEnumerable<User> Users { get; set; }
    }
}