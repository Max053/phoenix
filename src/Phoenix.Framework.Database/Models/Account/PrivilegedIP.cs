﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Phoenix.Framework.Database.Models.Account
{
    public class PrivilegedIP
    {
        public int ID { get; set; }
        [Required]
        public string IPBegin { get; set; }
        [Required]
        public string IPEnd { get; set; }
        [Required]
        public User GMAccount { get; set; }
        public string Description { get; set; }
        public DateTime IssueDateTime { get; set; }        
        public DateTime ExpirationDateTime { get; set; }

    }
}
