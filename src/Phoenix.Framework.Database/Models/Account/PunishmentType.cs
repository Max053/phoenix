﻿using System;

namespace Phoenix.Framework.Database.Models.Account
{
    [Flags]
    public enum PunishmentType : ulong
    {
        Login,
        Chat,
        Exchange,
        Party,
        Guild,
        Academy,
        PlayerKill
    }
}