﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Phoenix.Framework.Database.Models.Account
{
    public class Announcement
    {
        public int ID { get; set; }
        [MaxLength(80)]
        public string Title { get; set; }
        [MaxLength(1024)]
        public string Message { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime EditDatetime { get; set; }
    }
}
