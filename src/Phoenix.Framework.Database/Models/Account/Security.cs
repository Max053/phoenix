﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Phoenix.Framework.Database.Models.Account
{
    public class Security
    {
        public int ID { get; set; }
        [MaxLength(40)]
        public string Name { get; set; }
        [MaxLength(1024)]
        public string Description { get; set; }
        public bool IsDisabled { get; set; }
    }
}
