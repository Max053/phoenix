﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Phoenix.Framework.Database.Models.Account
{
    public class User
    {
        public int ID { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public string RegIP { get; set; }
        public DateTime RegDateTime { get; set; }
        public SecurityGroup SecurityGroup { get; set; }
    }
}
