﻿using Phoenix.Framework.Database.Models.Account;

namespace Phoenix.Framework.Database.Persistence.Repositories
{
    internal class UserRepository : Repository<User>, IUserRepository
    {
        public UserRepository(PhoenixContext context) : base(context)
        {
        }
    }
}