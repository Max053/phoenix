﻿using Phoenix.Framework.Database.Models.Account;

namespace Phoenix.Framework.Database.Persistence.Repositories
{
    internal interface ISecurityPolicyRepository : IRepository<SecurityPolicy>
    {
    }
}