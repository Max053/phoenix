﻿using Phoenix.Framework.Database.Models.Account;
using System;
using System.Collections.Generic;
using System.Text;

namespace Phoenix.Framework.Database.Persistence.Repositories
{
    public class SecurityPolicyRepository : Repository<SecurityPolicy>, ISecurityPolicyRepository
    {
        public SecurityPolicyRepository(PhoenixContext context) : base(context)
        {
        }
    }
}
