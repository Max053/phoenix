﻿using Microsoft.EntityFrameworkCore;
using Phoenix.Framework.Database.Models.Account;
using Phoenix.Framework.Database.Models.Logs;
using Phoenix.Framework.Database.Models.System;

namespace Phoenix.Framework.Database
{
    public class PhoenixContext : DbContext
    {
        #region Account

        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<SecurityGroup> SecurityGroups { get; set; }
        public virtual DbSet<SecurityPolicy> SecurityPolicies { get; set; }
        public virtual DbSet<SecurityGroupPolicies> SecurityGroupPolicies { get; set; }
        public virtual DbSet<Punishment> Punishments { get; set; }
        public virtual DbSet<Silk> Silks { get; set; }

        #endregion Account

        #region Logs

        public virtual DbSet<LoginHistory> LoginHistory { get; set; }

        #endregion Logs

        #region System

        public virtual DbSet<Announcement> Announcements { get; set; }
        public virtual DbSet<PrivilegedIP> PrivilegedIPs { get; set; }
        public virtual DbSet<ModuleVersion> ModuleVersions { get; set; }
        public virtual DbSet<ModuleVersionFile> ModuleVersionFiles { get; set; }

        #endregion System

        public PhoenixContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            #region Account

            builder.Entity<User>(entity =>
            {
                entity.ToTable("Users", "Account");
                entity.HasKey(e => e.ID);
                entity.HasOne(e => e.SecurityGroup);
                entity.Property(p => p.RegDateTime).HasDefaultValueSql("getdate()");
            });

            builder.Entity<SecurityGroup>(entity =>
            {
                entity.ToTable("SecurityGroups", "Account");
                entity.HasKey(e => e.ID);
                entity.HasMany(e => e.Users).WithOne(e => e.SecurityGroup);
                //entity.HasMany(e => e.SecurityPolicies);
            });

            builder.Entity<SecurityPolicy>(entity =>
            {
                entity.ToTable("SecurityPolicies", "Account");
                entity.HasKey(e => e.ID);
                //entity.HasMany(e => e.SecurityGroups);
            });

            builder.Entity<SecurityGroupPolicies>(entity =>
            {
                entity.ToTable("SecurityGroupPolicies", "Account");
                entity.HasKey(e => new { e.SecurityGroupID, e.SecurityPolicyID });
            });

            builder.Entity<Punishment>(entity =>
            {
                entity.ToTable("Punishments", "Account");
                entity.HasKey(e => e.ID);
            });

            builder.Entity<Announcement>(entity =>
            {
                entity.ToTable("Announcements", "Account");
                entity.HasKey(e => e.ID);
                entity.Property(p => p.EditDatetime).HasDefaultValueSql("getdate()");
            });

            builder.Entity<PrivilegedIP>(entity =>
            {
                entity.ToTable("PrivilegedIPs", "Account");
                entity.HasKey(e => e.ID);
                entity.Property(p => p.IssueDateTime).HasDefaultValueSql("getdate()");
                entity.Property(p => p.ExpirationDateTime).HasDefaultValueSql("DATEADD(MONTH, 6, GETDATE())");
            });

            builder.Entity<Silk>(entity =>
            {
                entity.ToTable("Silks", "Account");
                entity.HasKey(e => e.ID);
                entity.HasOne(e => e.User);
            });

            #endregion Account

            #region Logs

            builder.Entity<LoginHistory>(entity =>
            {
                entity.ToTable("LoginHistory", "Logs");
                entity.HasKey(e => e.ID);
                entity.Property(p => p.DateTime).HasDefaultValueSql("getdate()");
            });

            #endregion Logs

            #region System

            builder.Entity<ModuleVersion>(entity =>
            {
                entity.ToTable("ModuleVersions", "System");
                entity.HasKey(e => e.ID);
                entity.Property(p => p.RecordUpdateStamp).HasDefaultValueSql("getdate()");
            });

            builder.Entity<ModuleVersionFile>(entity =>
            {
                entity.ToTable("ModuleVersionFiles", "System");
                entity.HasKey(e => e.ID);
                entity.Property(p => p.RecordUpdateStamp).HasDefaultValueSql("getdate()");
            });

            #endregion System
        }
    }
}