﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Phoenix.Framework.Database;

namespace Phoenix.Framework.Database.Migrations
{
    [DbContext(typeof(PhoenixContext))]
    partial class PhoenixContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.2.4-servicing-10062")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Phoenix.Framework.Database.Models.Account.Announcement", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("EditDatetime")
                        .ValueGeneratedOnAddOrUpdate()
                        .HasDefaultValueSql("getdate()");

                    b.Property<string>("Message")
                        .HasMaxLength(1024);

                    b.Property<string>("Title")
                        .HasMaxLength(80);

                    b.HasKey("ID");

                    b.ToTable("Announcements","Account");
                });

            modelBuilder.Entity("Phoenix.Framework.Database.Models.Account.PrivilegedIP", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Description");

                    b.Property<DateTime>("ExpirationDateTime")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValueSql("DATEADD(MONTH, 6, GETDATE())");

                    b.Property<int>("GMAccountID");

                    b.Property<string>("IPBegin")
                        .IsRequired();

                    b.Property<string>("IPEnd")
                        .IsRequired();

                    b.Property<DateTime>("IssueDateTime")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValueSql("getdate()");

                    b.HasKey("ID");

                    b.HasIndex("GMAccountID");

                    b.ToTable("PrivilegedIPs","Account");
                });

            modelBuilder.Entity("Phoenix.Framework.Database.Models.Account.Punishment", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int?>("ExecutorID");

                    b.Property<string>("Reason")
                        .HasMaxLength(1024);

                    b.Property<DateTime>("TimeBegin");

                    b.Property<DateTime>("TimeEnd");

                    b.Property<decimal>("Type")
                        .HasConversion(new ValueConverter<decimal, decimal>(v => default(decimal), v => default(decimal), new ConverterMappingHints(precision: 20, scale: 0)));

                    b.Property<int?>("UserID");

                    b.HasKey("ID");

                    b.HasIndex("ExecutorID");

                    b.HasIndex("UserID");

                    b.ToTable("Punishments","Account");
                });

            modelBuilder.Entity("Phoenix.Framework.Database.Models.Account.SecurityGroup", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Description")
                        .HasMaxLength(1024);

                    b.Property<bool>("IsDisabled");

                    b.Property<string>("Name")
                        .HasMaxLength(40);

                    b.HasKey("ID");

                    b.ToTable("SecurityGroups","Account");
                });

            modelBuilder.Entity("Phoenix.Framework.Database.Models.Account.SecurityGroupPolicies", b =>
                {
                    b.Property<int>("SecurityGroupID");

                    b.Property<int>("SecurityPolicyID");

                    b.HasKey("SecurityGroupID", "SecurityPolicyID");

                    b.HasIndex("SecurityPolicyID");

                    b.ToTable("SecurityGroupPolicies","Account");
                });

            modelBuilder.Entity("Phoenix.Framework.Database.Models.Account.SecurityPolicy", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Description")
                        .HasMaxLength(1024);

                    b.Property<bool>("IsDisabled");

                    b.Property<string>("Name")
                        .HasMaxLength(40);

                    b.HasKey("ID");

                    b.ToTable("SecurityPolicies","Account");
                });

            modelBuilder.Entity("Phoenix.Framework.Database.Models.Account.Silk", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("Gift");

                    b.Property<int>("Own");

                    b.Property<int>("Point");

                    b.Property<int?>("UserID");

                    b.HasKey("ID");

                    b.HasIndex("UserID");

                    b.ToTable("Silks","Account");
                });

            modelBuilder.Entity("Phoenix.Framework.Database.Models.Account.User", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Email");

                    b.Property<string>("Name");

                    b.Property<string>("Password");

                    b.Property<DateTime>("RegDateTime")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValueSql("getdate()");

                    b.Property<string>("RegIP");

                    b.Property<int?>("SecurityGroupID");

                    b.Property<string>("Username");

                    b.HasKey("ID");

                    b.HasIndex("SecurityGroupID");

                    b.ToTable("Users","Account");
                });

            modelBuilder.Entity("Phoenix.Framework.Database.Models.Logs.LoginHistory", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("DateTime")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValueSql("getdate()");

                    b.Property<string>("HWID");

                    b.Property<string>("IP");

                    b.Property<int?>("UserID");

                    b.HasKey("ID");

                    b.HasIndex("UserID");

                    b.ToTable("LoginHistory","Logs");
                });

            modelBuilder.Entity("Phoenix.Framework.Database.Models.System.ModuleVersion", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Description");

                    b.Property<bool>("IsValid");

                    b.Property<int>("Module");

                    b.Property<string>("Name");

                    b.Property<DateTime>("RecordUpdateStamp")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValueSql("getdate()");

                    b.Property<int>("Version");

                    b.HasKey("ID");

                    b.ToTable("ModuleVersions","System");
                });

            modelBuilder.Entity("Phoenix.Framework.Database.Models.System.ModuleVersionFile", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("FileDest");

                    b.Property<string>("FileName");

                    b.Property<int>("FileSize");

                    b.Property<byte>("FileType");

                    b.Property<bool>("IsValid");

                    b.Property<int?>("ModuleVersionID");

                    b.Property<DateTime>("RecordUpdateStamp")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValueSql("getdate()");

                    b.Property<bool>("ToBePacked");

                    b.HasKey("ID");

                    b.HasIndex("ModuleVersionID");

                    b.ToTable("ModuleVersionFiles","System");
                });

            modelBuilder.Entity("Phoenix.Framework.Database.Models.Account.PrivilegedIP", b =>
                {
                    b.HasOne("Phoenix.Framework.Database.Models.Account.User", "GMAccount")
                        .WithMany()
                        .HasForeignKey("GMAccountID")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Phoenix.Framework.Database.Models.Account.Punishment", b =>
                {
                    b.HasOne("Phoenix.Framework.Database.Models.Account.User", "Executor")
                        .WithMany()
                        .HasForeignKey("ExecutorID");

                    b.HasOne("Phoenix.Framework.Database.Models.Account.User", "User")
                        .WithMany()
                        .HasForeignKey("UserID");
                });

            modelBuilder.Entity("Phoenix.Framework.Database.Models.Account.SecurityGroupPolicies", b =>
                {
                    b.HasOne("Phoenix.Framework.Database.Models.Account.SecurityGroup", "SecurityGroup")
                        .WithMany("SecurityGroupPolicies")
                        .HasForeignKey("SecurityGroupID")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Phoenix.Framework.Database.Models.Account.SecurityPolicy", "SecurityPolicy")
                        .WithMany("SecurityGroupPolicies")
                        .HasForeignKey("SecurityPolicyID")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Phoenix.Framework.Database.Models.Account.Silk", b =>
                {
                    b.HasOne("Phoenix.Framework.Database.Models.Account.User", "User")
                        .WithMany()
                        .HasForeignKey("UserID");
                });

            modelBuilder.Entity("Phoenix.Framework.Database.Models.Account.User", b =>
                {
                    b.HasOne("Phoenix.Framework.Database.Models.Account.SecurityGroup", "SecurityGroup")
                        .WithMany("Users")
                        .HasForeignKey("SecurityGroupID");
                });

            modelBuilder.Entity("Phoenix.Framework.Database.Models.Logs.LoginHistory", b =>
                {
                    b.HasOne("Phoenix.Framework.Database.Models.Account.User", "User")
                        .WithMany()
                        .HasForeignKey("UserID");
                });

            modelBuilder.Entity("Phoenix.Framework.Database.Models.System.ModuleVersionFile", b =>
                {
                    b.HasOne("Phoenix.Framework.Database.Models.System.ModuleVersion", "ModuleVersion")
                        .WithMany("ModuleVersionFiles")
                        .HasForeignKey("ModuleVersionID");
                });
#pragma warning restore 612, 618
        }
    }
}
