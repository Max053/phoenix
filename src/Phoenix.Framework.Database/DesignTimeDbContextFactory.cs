﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using System;
using System.Collections.Generic;
using System.Text;

namespace Phoenix.Framework.Database
{
    internal class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<PhoenixContext>
    {
        private const string connectionString = "Server=.;Database=Phoenix;Trusted_Connection=True;";
        public PhoenixContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<PhoenixContext>();
            builder.UseSqlServer(connectionString);
            var context = new PhoenixContext(builder.Options);

#if DEBUG
            context.Database.Migrate(); //TODO: Do we want to auto migrate the DB?
#endif 

            return context;
        }
    }
}
